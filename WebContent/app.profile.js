var profile = {
	// `basePath` is relative to the directory containing this profile file; in this case, it is being set to the src directory i.e. location where entire application code exists 
	basePath: 'src',
	
// This is the path where the build output will be written and can be configured as ‘releaseDir’ 
	releaseDir: "../release",

// it is the name of the output directory i.e. release name of the application build
	releaseName: "dojoApp",

// Builds a new release.
	action: 'release',
// Strips all comments and whitespace from CSS files and inlines @imports where possible.
	cssOptimize: 'comments',

// Excludes tests, demos, and original template files from being included in the built version.
	mini: true,

// Uses Closure Compiler as the JavaScript minifier. This can also be set to "shrinksafe" to use ShrinkSafe

// This option defaults to "" (no compression) if not provided.
	optimize: "shrinksafe",

// We're building layers, so we need to set the minifier to use for those, too.
// This defaults to "shrinksafe" if not provided.
	layerOptimize: "shrinksafe",

	// A list of packages that will be built. The same packages defined in the loader (i.e. index.html) should be defined here in the
	// build profile.
packages: [
		// Using a string as a package is shorthand for `{ name: 'app', location: 'app' }`
			'dojo',
        		'dijit',
        		'app'
	],

	// Strips all calls to console functions within the code. You can also set this to "warn" to strip everything
	// but console.error, and any other truthy value to strip everything but console.warn and console.error.
	// This defaults to "normal" (strip all but warn and error) if not provided.
	stripConsole: 'all',

	// The default selector engine is not included by default in a dojo.js build in order to make mobile builds
	// smaller. We add it back here to avoid that extra HTTP request. There is also an "acme" selector available; if
	// you use that, you will need to set the `selectorEngine` property in index.html, too.
	selectorEngine: 'lite',

	// Any module in an application can be converted into a "layer" module, which consists of the original module +
	// additional dependencies built into the same file. Using layers allows applications to reduce the number of HTTP
	// requests by combining all JavaScript into a single file.
	layers: {
		// This is the main loader module. It is a little special because it is treated like an AMD module even though
		// it is actually just plain JavaScript. There is some extra magic in the build system specifically for this module ID.
		'dojo/dojo': {
			// By default, the build system will try to include `dojo/main` in the built `dojo/dojo` layer, which adds a bunch of stuff we do not want or need. We want the initial script load to be as small and quick to load as possible, so we configure it as a custom, bootable base.
			include:["dojo/_base/config","dojo/json","dojo/domReady"],
boot: true,
			customBase: true
		},

		// In this demo application, we load `app/main` on the client-side, so here we build a separate layer containing that code. Note that when you create a new layer, the module referenced by the layer is always included in the layer (in this case, `app/main`)
	'app/main': {include:["dijit/registry","dijit/layout/BorderContainer",
            	         "dijit/layout/TabContainer","dijit/layout/ContentPane","dijit/Dialog"]}
	},
};


